import React from 'react'; 
import {FaReact,FaCss3, FaHtml5 ,FaPython, FaGitAlt, FaGitlab, FaNpm, FaBootstrap} from "react-icons/fa";
import {DiNodejs, DiJavascript1} from "react-icons/di";
import {SiExpress, SiMongodb, SiPostman, SiVercel} from "react-icons/si";

const Skills = ({skill}) => {
    const icon = {
      Postman: <SiPostman/>,
      React: <FaReact/>,
      Javascript: <DiJavascript1/>,
      Node : <DiNodejs/>,
      Express : <SiExpress/>,
      MongoDb : <SiMongodb/>,
      Git : <FaGitAlt/>,
      Gitlab : <FaGitlab/>,
      Npm: <FaNpm />,
      Python: <FaPython />,
      Bootstrap: <FaBootstrap/>,
      Vercel: <SiVercel />,
      Html5: <FaHtml5 />,
      Css3: <FaCss3/>,
    }
    
  return (
    <div title={skill} className='SkillBox'>
      {icon[skill]}
    </div>
  )
}

export default Skills
