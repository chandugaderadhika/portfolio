import React from 'react';
import ProjectBox from './ProjectBox';
import Portfolio from '../images/Portfolio.jpg';
import Fullstack from '../images/Fullstack.jpg';

const Projects = () => {
  return (
    <div>
      <h1 className='projectHeading'>My <b>Projects</b></h1>
      <div className='project'>
        <ProjectBox projectPhoto={Portfolio} projectName="portfolio" />
        <ProjectBox projectPhoto={Fullstack} projectName="ecom" />
      </div>

    </div>
  )
}

export default Projects