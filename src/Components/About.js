import React from 'react';
import Skills from './Skills.js';
import Tilt from 'react-parallax-tilt';
import Avatar from '../images/Avatar.png';


const About = () => {
  return (
    <>
      <div className='AboutPage'>
        <div className='AboutText'>
          <h1 className='AboutTextHeading' >Get to <b>know</b> me!</h1>
          <p>
            Hi, my name is <b>Radhika Chandugade</b> and I am from Mumbai, India.
            I'm a <b>Full stack web developer</b> and a 2023 passout <b>BE in IT</b>. <br/><br/>
            I have done an internship as a <b>python developer</b> at moryya which is a fintech startup.
            I love to create original projects with beautiful designs, you can check out some of my work in the projects section.<br/><br/>
            I am <b>open</b> to new collaborations or work where I can contribute and grow. Feel free to connect with me, links are in the footer.<br/>
            Apart from coding I love to do drawing, you can check out some of my work here <a href="https://www.instagram.com/street_wali_artist?igsh=Y3l4OGp3MGhxeXl6" target='_blank'>Instagram.</a>
          </p>
        </div>

       <Tilt>
          <img className='Avatar' src={Avatar} alt="" />
        </Tilt>

      </div>
      
      <h1 className='SkillsHeading'>Professional Skillset</h1>
      <div className='skills'>
        
        <Skills skill='React'/>
        <Skills skill='Node' />
        <Skills skill='Express' />
        <Skills skill='MongoDb' />
        <Skills skill='Git' />
        <Skills skill='Gitlab' />
        <Skills skill='Javascript' />
        <Skills skill='Postman' />
        <Skills skill='Python' />
        <Skills skill='Vercel' />
        <Skills skill='Npm' />
        <Skills skill='Bootstrap' />
        <Skills skill='Html5'/>
        <Skills skill='Css3'/>
        
        
      </div>
    </>
  )
}

export default About