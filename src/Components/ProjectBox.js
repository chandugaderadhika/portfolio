import React from 'react';
import { FaGitlab } from 'react-icons/fa';
import { CgFileDocument } from 'react-icons/cg';

const ProjectBox = ({ projectPhoto, projectName }) => {
  const projects = {
    ecom: {
      desc:
        'I developed a comprehensive full-stack JavaScript websit.',
      gitlab: 'https://gitlab.com/chandugaderadhika/Ecom.git',
      demo: 'http://194.195.117.91/',
    },
    portfolio: {
      desc: 'A website that shows about my portfolio',
      gitlab: 'https://gitlab.com/chandugaderadhika/portfolio.git',
      demo: 'https://radhika-portfolio.vercel.app/',
    },
  };

  const projectInfo = projects[projectName];

  const handleGitlabClick = () => {
    console.log('Gitlab button clicked');
    window.open(projectInfo.gitlab, '_blank');
  };

  const handleDemoClick = () => {
    console.log('Demo button clicked');
    window.open(projectInfo.demo, '_blank');
  };

  return (
    <div className='projectBox'>
      <img className='projectPhoto' src={projectPhoto} alt='Project display' />
      <div>
        <br />
        <h3>{projectName}</h3>
        <br />
        {projectInfo.desc}
        <br />

        <a
          href={projectInfo.gitlab}
          target='_blank'
          rel='noopener noreferrer'
          onClick={handleGitlabClick}
        >
          <button className='projectbtn'>
            <FaGitlab /> Gitlab
          </button>
        </a>

        <a
          href={projectInfo.demo}
          target='_blank'
          rel='noopener noreferrer'
          onClick={handleDemoClick}
        >
          <button className='projectbtn'>
            <CgFileDocument /> Demo
          </button>
        </a>
      </div>
    </div>
  );
};

export default ProjectBox;
