import React from 'react';
import {FaGithub, FaLinkedin} from "react-icons/fa";
import {GrMail} from "react-icons/gr";

const Footer = () => {
  return (
    <footer>
      <h4>Developed by Radhika Chandugade</h4>
      <h4>Copyright &copy; 2023 RC</h4>
      <div className='footerLinks'>
        <a href="https://github.com/Radhikachandugade" target='_blank'><FaGithub/></a>
        <a href="https://www.linkedin.com/in/radhika-chandugade-97815a214/" target='_blank'><FaLinkedin/></a>
        <a href='mailTo:chandugaderadhika@gmail.com' target='_blank'><GrMail/></a>
      </div>
    </footer>
  )
}

export default Footer